/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// Modules
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CoreModule} from './@core/core.module';
import {ModalModule} from './pages/modals/modals.module';
import {MailModule} from './pages/mail/mail.module';
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthModule} from './pages/auth/auth.module';

import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
// Services
import {ApiService} from './services/api-service';
import {DataService} from './services/data-service';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';
import {AuthService} from './services/auth-service';
// Components
import {AppComponent} from './app.component';
import {AuthGuard} from './guards/auth-guard';
import {AgmCoreModule} from '@agm/core';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AuthModule,
    AppRoutingModule,
    AngularMultiSelectModule,
    NguiAutoCompleteModule,
    MailModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDHpm1LS7z3pbxK6FhsT5Y4p7I6WAuv_kg',
    }),
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    ModalModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    ApiService,
    DataService,
    AuthService,
    AuthGuard,
    {provide: APP_BASE_HREF, useValue: '/'},
  ],
})
export class AppModule {
}
