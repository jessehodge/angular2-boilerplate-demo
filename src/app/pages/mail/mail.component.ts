import {Component, OnInit} from '@angular/core';

import {MailService} from './mail.service';

@Component({
  selector: 'ngx-mail',
  templateUrl: './mail.component.html',
})

export class MailComponent implements OnInit {

  constructor(private mailService: MailService) {
  }

  ngOnInit() {
    const id = localStorage.getItem('user_id');
    this.mailService.getMail(id).subscribe();
  }

}
