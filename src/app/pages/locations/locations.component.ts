import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-locations',
  template: `
    <router-outlet></router-outlet>`,
  styleUrls: ['./locations.component.css'],
})
export class LocationsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
