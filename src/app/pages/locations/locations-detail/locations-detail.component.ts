import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationService} from '../locations.service';
import {Locations} from '../../../models/locations/locations';
import {LocalDataSource} from 'ng2-smart-table';
import {PartService} from '../../parts/parts.service';

@Component({
  selector: 'ngx-locations-detail',
  templateUrl: './locations-detail.component.html',
})

export class LocationsDetailComponent implements OnInit {
  @Input() id: number;
  public location = new Locations();
  public parts = [];
  public source: LocalDataSource = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: true, edit: true, delete: false, custom: ['pull']},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    pull: {title: 'Pull'},
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      unit_cost: {title: 'Cost'}, qty: {title: 'Qty'},
    },
  };

  constructor(private activatedRoute: ActivatedRoute, private locationService: LocationService, private partService: PartService,
              private router: Router) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.locationService.getLocation(this.id).subscribe(
      res => {
        this.location = res;
        this.source.load(res.parts);
      });
  }

  editLocation() {
    this.router.navigate(['/pages/locations/location_edit/', this.id], {relativeTo: this.activatedRoute});
  }

  onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  onCreateConfirm(event) {
    event.newData['i_location_id'] = this.id;
    this.partService.postPart(event.newData).subscribe(
      () => {
        event.confirm.resolve();
      },
      () => {
        event.confirm.reject();
      });
  }

}
