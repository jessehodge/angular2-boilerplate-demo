import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LocationService} from './locations.service';
import {LocationRoutes} from './locations.routes';
import {LocationsComponent} from './locations.component';
import {LocationsCreationComponent} from './locations-creation/locations-creation.component';
import {LocationsDetailComponent} from './locations-detail/locations-detail.component';
import {LocationsTableComponent} from './locations-table/locations-table.component';
import {LocationsEditComponent} from './locations-edit/locations-edit.component';
import {YardService} from '../yards/yards.service';
import {PartService} from '../parts/parts.service';

import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    LocationRoutes,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
  ],
  declarations: [
    LocationsComponent,
    LocationsCreationComponent,
    LocationsDetailComponent,
    LocationsTableComponent,
    LocationsEditComponent,
  ],
  providers: [
    LocationService,
    YardService,
    PartService,
  ],
})
export class LocationsModule {
}
