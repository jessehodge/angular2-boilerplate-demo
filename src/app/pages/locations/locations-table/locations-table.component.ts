import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Locations} from '../../../models/locations/locations';
import {LocationService} from '../locations.service';
import {LocalDataSource} from 'ng2-smart-table';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'ngx-locations-table',
  templateUrl: './locations-table.component.html',
})
export class LocationsTableComponent implements OnInit, OnDestroy {
  public locations = new Array<Locations>();
  public source: LocalDataSource = new LocalDataSource();
  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'}, yard_id: {title: 'Description'},
      description: {title: 'Description'},
    },
  };
  private ngUnsubscribe: Subject<Locations> = new Subject();

  constructor(private locationService: LocationService,
              private _router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.locationService.getLocations()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          this.locations = res;
          this.source.load(this.locations);
        });
  }

  onLocationClick(event) {
    this._router.navigate(['/pages/locations/location_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
