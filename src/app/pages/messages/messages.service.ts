import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class MessageService {
  private message_url = this.data.base_url + '/message/';

  constructor(private data: DataService) {
  }

  public postMessage(message) {
    return this.data.post(this.message_url, message);
  }
}
