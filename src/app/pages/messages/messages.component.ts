import {Component, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MessageService} from './messages.service';
import {ApiService} from '../../services/api-service';
import {Messages} from '../../models/messages/messages';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-messages',
  templateUrl: 'messages.component.html',
})
export class MessagesComponent implements OnDestroy {
  public message = new Messages();
  @Input() messages;
  public id: number;
  public index;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private messageService: MessageService,
              private apiService: ApiService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params
      .takeUntil(this.ngUnsubscribe)
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  postMessage(message) {
    message.sender = localStorage.getItem('user_id');
    message.workorder_id = this.id;
    this.messageService.postMessage(message).takeUntil(this.ngUnsubscribe).subscribe();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
