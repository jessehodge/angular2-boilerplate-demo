import {Component, OnInit} from '@angular/core';
import {TimeclockService} from '../timeclock.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Timesheet} from '../../../models/timesheet/timesheet';

@Component({
  selector: 'ngx-timesheet-table',
  templateUrl: './timesheet-detail.component.html',
  styleUrls: ['./timesheet-detail.component.css'],
})

export class TimesheetDetailComponent implements OnInit {
  private id: number;
  public timesheet = new Timesheet();
  public long: number;
  public lat: number;
  public timeInLoc: string;
  public timeInLocLong: number;
  public timeInLocLat: number;
  public loLoc: string;
  public loLocLat: number;
  public loLocLong: number;
  public liLoc: string;
  public liLocLat: number;
  public liLocLong: number;
  public toLoc: string;
  public toLocLat: number;
  public toLocLong: number;

  constructor(private timeService: TimeclockService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.timeService.getTimesheet(this.id).subscribe(timesheet => {
      this.timeInLoc = timesheet.tiLoc.split(', ');
      this.loLoc = timesheet.loLoc.split(', ');
      this.liLoc = timesheet.liLoc.split(', ');
      this.toLoc = timesheet.toLoc.split(', ');
      this.timeInLocLat = Number(this.timeInLoc[0]);
      this.timeInLocLong = Number(this.timeInLoc[1]);
      this.loLocLat = Number(this.loLoc[0]);
      this.loLocLong = Number(this.loLoc[1]);
      this.liLocLat = Number(this.liLoc[0]);
      this.liLocLong = Number(this.liLoc[1]);
      this.toLocLat = Number(this.toLoc[0]);
      this.toLocLong = Number(this.toLoc[1]);
      this.timesheet = timesheet;
    });
  }

  public clockedInMarker() {
    this.lat = this.timeInLocLat;
    this.long = this.timeInLocLong;
  }

  public clockOutForLunchMarker() {
    this.lat = this.loLocLat;
    this.long = this.loLocLong;
  }

  public clockInForLunchMarker() {
    this.lat = this.liLocLat;
    this.long = this.liLocLong;
  }

  public clockedOutMarker() {
    this.lat = this.loLocLat;
    this.long = this.loLocLong;
  }

}
