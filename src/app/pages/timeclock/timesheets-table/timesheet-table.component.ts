import { Component, OnInit } from '@angular/core';
import {TimeclockService} from '../timeclock.service';
import {LocalDataSource} from 'ng2-smart-table';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-timesheet-table',
  templateUrl: './timesheet-table.component.html',
})
export class TimesheetTableComponent implements OnInit {
  public timesheet_source = new LocalDataSource();

  constructor(private timeService: TimeclockService, private router: Router, private activatedRoute: ActivatedRoute) { }

  timesheet_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      date: {title: 'Date'},
      timeIn: {title: 'Clocked In'},
      lunchIn: {title: 'Lunch Start'},
      lunchOut: {title: 'Lunch Stop'},
      timeOut: {title: 'Clocked Out'},
      totalTime: {title: 'Total Time'},
    },
  };

  ngOnInit() {
    this.timeService.getTimesheets().subscribe(res => this.timesheet_source.load(res));
  }

  public onTimesheetClick(event) {
    this.router.navigate(['/pages/timeclock/time_sheet_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }
}
