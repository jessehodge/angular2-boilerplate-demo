import { Injectable } from '@angular/core';
import { DataService } from '../../services/data-service';

@Injectable()
export class TimeclockService {

  public _timesheets_url = this.data.base_url + '/timesheets/';
  public _timesheet_url = this.data.base_url + '/timesheet/';

  constructor(private data: DataService) { }

  getTimesheets() {
    const url = this._timesheets_url;
    return this.data.get(url);
  }

  getTimesheet(id) {
    const url = this._timesheet_url + id + '/';
    return this.data.get(url);
  }

}
