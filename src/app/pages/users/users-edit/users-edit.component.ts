import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfileService} from '../../profile/profile.service';
import {UsersService} from '../users.service';
import {User} from '../../../models/user/user';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {JobsService} from '../../jobs/jobs.service';

@Component({
  selector: 'ngx-users-edit',
  templateUrl: './users-edit.component.html',
})
export class UsersEditComponent implements OnInit, OnDestroy {
  public id: number;
  public user = new User();
  public jobs = [];
  public roles = [];
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private profileService: ProfileService,
              private usersService: UsersService,
              private jobService: JobsService) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.usersService.getUser(this.id).takeUntil(this.ngUnsubscribe).subscribe(user_res => this.user = user_res);
    this.jobService.getJobs().takeUntil(this.ngUnsubscribe).subscribe(job_res => this.jobs = job_res);
  }

  submitProfile() {
    this.user.id = this.id;
    this.profileService.updateUser(this.user).subscribe(
      () => {
        alert('User was updated successfully!');
        this.router.navigate(['/pages/users/detail/', this.id, {relativeTo: this.activatedRoute}]);
      });
  }

  resetUserPassword() {
    this.user.password = 'Pumpco123';
    this.user.password_confirmation = 'Pumpco123';
    this.profileService.updateUser(this.user);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
