import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class UsersService {

  private _users_url = this.data.base_url + '/users/';
  private _operator_url = this.data.base_url + '/role/2/';
  private _mechanic_url = this.data.base_url + '/role/3/';

  constructor(private data: DataService) {
  }

  getUsers() {
    const url = this._users_url;
    return this.data.get(url);
  }

  getUser(id) {
    const url = this._users_url + id;
    return this.data.get(url);
  }

  getMechanics() {
    const url = this._mechanic_url;
    return this.data.get(url);
  }

  getOperators() {
    const url = this._operator_url;
    return this.data.get(url);
  }

}
