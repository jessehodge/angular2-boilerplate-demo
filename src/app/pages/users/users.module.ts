import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UserRouting} from './users.routes';
import {UsersService} from './users.service';
import {ProfileService} from '../profile/profile.service';
import {ApiService} from '../../services/api-service';
import {ThemeModule} from '../../@theme/theme.module';

import {Ng2SmartTableModule} from 'ng2-smart-table';
import {UsersComponent} from './users.component';
import {UsersDetailComponent} from './users-detail/users-detail.component';
import {UsersEditComponent} from './users-edit/users-edit.component';
import {UsersTableComponent} from './users-table/users-table.component';

@NgModule({
  imports: [
    CommonModule,
    UserRouting,
    Ng2SmartTableModule,
    FormsModule,
    ThemeModule,
  ],
  declarations: [UsersComponent, UsersDetailComponent, UsersEditComponent, UsersTableComponent],
  providers: [UsersService, ProfileService, ApiService],
})
export class UsersModule {
}
