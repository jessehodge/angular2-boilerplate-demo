import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../models/user/user';
import {UsersService} from '../users.service';
import {ProfileService} from '../../profile/profile.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-users-detail',
  templateUrl: './users-detail.component.html',
})

export class UsersDetailComponent implements OnInit, OnDestroy {
  public user = new User();
  public id: number;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private usersService: UsersService,
              private profileService: ProfileService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.usersService.getUser(this.id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          this.user = res;
        });
  }

  editUser() {
    this.router.navigate(['/pages/users/edit/', this.user.id]);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
