import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users.component';
import {UsersTableComponent} from './users-table/users-table.component';
import {UsersDetailComponent} from './users-detail/users-detail.component';
import {UsersEditComponent} from './users-edit/users-edit.component';

const routes: Routes = [
  {
    path: '', component: UsersComponent, children: [
      {path: '', component: UsersTableComponent},
      {path: 'detail/:id', component: UsersDetailComponent},
      {path: 'edit/:id', component: UsersEditComponent},
    ],
  }];

export const UserRouting = RouterModule.forChild(routes);
