import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {YardService} from '../yards.service';
import {LocalDataSource} from 'ng2-smart-table';

@Component({
  selector: 'ngx-yards-table',
  templateUrl: './yards-table.component.html',
})
export class YardsTableComponent implements OnInit {
  public id: number;
  public source: LocalDataSource = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      name: {title: 'Name:'}, city: {title: 'City'},
    },
  };

  constructor(private yardService: YardService, private _router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.yardService.getYards().subscribe(res => this.source.load(res.yards));
  }

  public onYardClick(event) {
    this._router.navigate(['yards_detail/', event.data.id], {relativeTo: this.activatedRoute}).catch();
  }
}
