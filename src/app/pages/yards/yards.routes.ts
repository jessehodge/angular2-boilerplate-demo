import {RouterModule, Routes} from '@angular/router';
import {YardsComponent} from './yards.component';
import {YardsCreationComponent} from './yards-creation/yards-creation.component';
import {YardsDetailComponent} from './yards-detail/yards-detail.component';
import {YardsEditComponent} from './yards-edit/yards-edit.component';
import {YardsTableComponent} from './yards-table/yards-table.component';

const routes: Routes = [
  {
    path: '',
    component: YardsComponent, children: [
      {path: '', component: YardsTableComponent},
      {path: 'yards_detail/:id', component: YardsDetailComponent},
      {path: 'yards_creation', component: YardsCreationComponent},
      {path: 'yards_edit/:id', component: YardsEditComponent},
    ],
  }];

export const YardRouting = RouterModule.forChild(routes);
