import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PartService} from '../parts.service';
import {Parts} from '../../../models/parts/parts';

@Component({
  selector: 'ngx-parts-order-detail',
  templateUrl: './parts-order-detail.component.html',
})
export class PartsOrderDetailComponent implements OnInit {
  @Input() id: number;
  public part = new Parts();
  public pullingPart = false;

  constructor(private partService: PartService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.partService.getPart(this.id).subscribe(res => this.part = res);
  }
}
