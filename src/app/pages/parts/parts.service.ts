import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class PartService {

  private _parts_url = this.data.base_url + '/parts/';
  private _part_url = this.data.base_url + '/part/';
  private _receive_parts_url = this.data.base_url + '/parts/receive/';
  private _ordered_parts_list = this.data.base_url + '/parts/ordered_list/';
  private _received_parts_url = this.data.base_url + '/parts/received/';
  private _ordered_parts_url = this.data.base_url + '/parts/ordered';
  private _requested_parts_url = this.data.base_url + '/parts/requested';
  private _inv_parts_url = this.data.base_url + '/parts/search/';

  constructor(private data: DataService) {
  }

  getParts() {
    const url = this._parts_url;
    return this.data.get(url);
  }

  getRequestedParts() {
    const url = this._requested_parts_url;
    return this.data.get(url);
  }

  getOrderedParts() {
    const url = this._ordered_parts_url;
    return this.data.get(url);
  }

  getReceivedParts() {
    const url = this._received_parts_url;
    return this.data.get(url);
  }

  getInvParts(part_number_desc) {
    const url = this._inv_parts_url + part_number_desc + '/';
    return this.data.get(url);
  }

  postPart(part) {
    const url = this._part_url;
    return this.data.post(url, part);
  }

  editPart(part) {
    const url = this._part_url + part.id + '/';
    return this.data.put(url, part);
  }

  getPart(id) {
    const url = this._part_url + id + '/';
    return this.data.get(url);
  }

  getPartCount() {
    const url = this._ordered_parts_list;
    return this.data.get(url);
  }

}
