import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PartService} from '../parts.service';
import {LocationService} from '../../locations/locations.service';
import {Parts} from '../../../models/parts/parts';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-parts-order-creation',
  templateUrl: './parts-order-creation.component.html',
})
export class PartsOrderCreationComponent implements OnDestroy {
  public part = new Parts();
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private partService: PartService, private locationService: LocationService, private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  submitPart() {
    this.partService.postPart(this.part).takeUntil(this.ngUnsubscribe).subscribe(() => this.router.navigate(['/pages/parts']));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
