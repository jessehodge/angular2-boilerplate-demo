import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {PartsRoutes} from './parts.routes';
import {PartService} from './parts.service';
import {PartsComponent} from './parts.component';
import {PartsOrderCreationComponent} from './parts-order-creation/parts-order-creation.component';
import {PartsOrderDetailComponent} from './parts-order-detail/parts-order-detail.component';
import {PartsTableComponent} from './parts-table/parts-table.component';
import {ThemeModule} from '../../@theme/theme.module';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';
import {LocationService} from '../locations/locations.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    PartsRoutes,
    ThemeModule,
    NguiAutoCompleteModule,
  ],
  declarations: [PartsComponent, PartsOrderCreationComponent, PartsOrderDetailComponent, PartsTableComponent],
  providers: [PartService, LocationService],
})
export class PartsModule {
}
