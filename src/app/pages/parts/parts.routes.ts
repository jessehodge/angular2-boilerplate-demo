import {RouterModule, Routes} from '@angular/router';
import {PartsComponent} from './parts.component';
import {PartsTableComponent} from './parts-table/parts-table.component';
import {PartsOrderCreationComponent} from './parts-order-creation/parts-order-creation.component';
import {PartsOrderDetailComponent} from './parts-order-detail/parts-order-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PartsComponent, children: [
      {path: '', component: PartsTableComponent},
      {path: 'part_creation', component: PartsOrderCreationComponent},
      {path: 'part_detail/:id', component: PartsOrderDetailComponent},
      {path: 'part_creation/unit/:unit', component: PartsOrderCreationComponent},
      {path: 'part_creation/location/:location', component: PartsOrderCreationComponent},
      {path: 'part_creation/work_order/:work_order', component: PartsOrderCreationComponent},
      {path: 'part_creation/po/:purchaseorder', component: PartsOrderCreationComponent},
    ],
  }];

export const PartsRoutes = RouterModule.forChild(routes);
