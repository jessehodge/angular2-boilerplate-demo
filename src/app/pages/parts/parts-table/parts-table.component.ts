import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PartService} from '../parts.service';
import {LocalDataSource} from 'ng2-smart-table';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-parts-table',
  templateUrl: './parts-table.component.html',
})
export class PartsTableComponent implements OnInit, OnDestroy {
  public source: LocalDataSource = new LocalDataSource();
  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {number: {title: 'Part #:'}, description: {title: 'Description'}, qty: {title: 'Quantity'}},
  };
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private partService: PartService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.partService.getParts()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.source.load(res);
      });
  }

  public onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
