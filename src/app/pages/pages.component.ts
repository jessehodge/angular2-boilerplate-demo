import {Component} from '@angular/core';
import {MENU_ITEMS} from './pages-menu';
import {AuthService} from '../services/auth-service';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu *ngIf='auth.loggedIn()' [items]='menu'></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;

  constructor(public auth: AuthService) {
  }
}
