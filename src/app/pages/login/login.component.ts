import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api-service';
import {Router} from '@angular/router';
import {User} from '../../models/user/user';
import 'rxjs/add/operator/takeUntil';
import {AuthService} from '../../services/auth-service';

@Component({
  selector: 'ngx-auth-login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss'],
})
export class LoginComponent {

  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted = false;
  public user = new User();

  constructor(fb: FormBuilder, private apiService: ApiService, private router: Router, private auth: AuthService) {
    this.form = fb.group({
      'email': ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+'),
        ])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
    localStorage.clear();
    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  public onSubmit(user) {
    this.submitted = true;
    if (this.form.valid) {
      this.auth.signIn(user)
        .subscribe(response => {
            if (response.status === 200) {
              const res = response.json();
              localStorage.setItem('token', res.token);
              localStorage.setItem('user_roles', res.user.roles);
              localStorage.setItem('user_id', res.user.id);
              localStorage.setItem('uid', res.user.email);
              this.router.navigate(['/pages/dashboard']);
            }
          },
          error2 => {
            if (error2.status === 401) {
              alert('Please enter the correct email and password.');
            }
          });
    }
  }
}
