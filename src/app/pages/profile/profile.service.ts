import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class ProfileService {

  private _register_user = this.data.base_url + '/users/';
  private _auth_user_url = this.data.base_url + '/user/';

  constructor(private data: DataService) {
  }

  registerUser(user) {
    const url = this._register_user;
    return this.data.post(url, user);
  }

  getUser(id) {
    const url = this._auth_user_url + id + '/';
    return this.data.get(url);
  }

  updateUser(user) {
    const url = this._auth_user_url + user.id + '/';
    return this.data.put(url, user);
  }

}
