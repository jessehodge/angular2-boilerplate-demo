import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ThemeModule} from '../../@theme/theme.module';

import {ProfileRouting} from './profile.routes';
import {ProfileService} from './profile.service';

import {ProfileComponent} from './profile.component';
import {ProfileDetailComponent} from './profile-detail/profile-detail.component';
import {ProfileEditComponent} from './profile-edit/profile-edit.component';
import {LogsService} from '../logs/logs.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProfileRouting,
    ThemeModule,
  ],
  declarations: [
    ProfileComponent,
    ProfileDetailComponent,
    ProfileEditComponent,
  ],
  providers: [ProfileService, LogsService],
})
export class ProfileModule {
}
