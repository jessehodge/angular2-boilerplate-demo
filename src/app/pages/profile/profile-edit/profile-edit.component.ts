import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../../models/user/user';
import {ProfileService} from '../profile.service';
import {AuthService} from '../../../services/auth-service';
import {JobsService} from '../../jobs/jobs.service';

@Component({
  selector: 'ngx-profile-edit',
  templateUrl: './profile-edit.component.html',
})

export class ProfileEditComponent implements OnInit {
  public id = localStorage.getItem('user_id');
  public job = localStorage.getItem('user_job_name');
  public old_password: string;
  public new_password: string;
  public confirmation_password: string;
  public wants_to_change_password = false;
  public user = new User();
  public jobs = [];

  constructor(private profileService: ProfileService,
              private router: Router,
              private auth: AuthService,
              private jobService: JobsService) {
  }

  ngOnInit() {
    this.profileService.getUser(this.id).subscribe(user_res => this.user = user_res);
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
  }

  changePassword() {
    if (this.wants_to_change_password === false) {
      this.wants_to_change_password = true;
    } else {
      this.wants_to_change_password = false;
    }
  }

  submitChangePassword() {
    this.user.password = this.old_password;
    if (this.confirmation_password === this.new_password) {
      this.auth.signIn(this.user);
    } else {
      alert('New password and confirmation do not match!');
    }
    this.wants_to_change_password = false;
  }

  submitProfile() {
    this.user.user_status_id = 1;
    if (this.user === undefined) {
      this.user.id = parseInt(localStorage.getItem('user_id'), 0);
    }
    if (this.user.job === undefined) {
      this.user.job.id = +localStorage.getItem('user_job_id');
    }
    this.profileService.updateUser(this.user);
    this.router.navigate(['/pages/profile/profile_detail'], this.user.id);
  }
}
