import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {ProfileDetailComponent} from './profile-detail/profile-detail.component';
import {ProfileEditComponent} from './profile-edit/profile-edit.component';

const routes: Routes = [
  {
    path: '', component: ProfileComponent, children: [
      {path: '', component: ProfileDetailComponent},
      {path: 'profile_detail/:id', component: ProfileDetailComponent},
      {path: 'profile_edit/:id', component: ProfileEditComponent},
    ],
  }];

export const ProfileRouting = RouterModule.forChild(routes);
