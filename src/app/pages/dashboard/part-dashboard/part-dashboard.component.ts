import {Component, OnInit} from '@angular/core';
import {OrderPartModalComponent} from '../../modals/parts/order-part/order-part.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalService} from '../../modals/modals.service';
import {PartService} from '../../parts/parts.service';
import {Prefs} from '../../../models/preferences/prefs';
import {SettingsService} from '../../settings/settings.service';
import {AssignPartModalComponent} from '../../modals/parts/assign-part/assign-part.modal';
import {LocalDataSource} from 'ng2-smart-table';

@Component({
  selector: 'ngx-part-dashboard',
  templateUrl: './part-dashboard.component.html',
})
export class PartDashboardComponent implements OnInit {
  public prefs = new Prefs();
  public requested_parts_source = new LocalDataSource();
  public ordered_parts_source = new LocalDataSource();
  public received_parts_source = new LocalDataSource();

  requested_parts_settings = {
    pager: {perPage: 20}, endPoint: 'http://www.pumpcorepair.com/parts/requested',
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {description: {title: 'Description'}, qty: {title: 'Quantity'}},
  };

  ordered_parts_settings = {
    pager: {perPage: 20}, actions: {add: false, delete: false, edit: true},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    columns: {
      unit_id: {title: 'Unit'},
      number: {title: 'Part #:', editable: false},
      description: {title: 'Description', editable: false},
      qty: {title: 'Quantity', editable: false},
    },
  };

  received_parts_settings = {
    pager: {perPage: 20}, actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {number: {title: 'Part #:'}, description: {title: 'Description'}, qty: {title: 'Quantity'}},
  };

  constructor(private modalService: NgbModal, private modalsService: ModalService, private partService: PartService,
              private settingService: SettingsService) {
  }

  ngOnInit() {
    const id = localStorage.getItem('user_id');
    this.settingService.getPrefs(id).subscribe(res => {
      this.prefs = res;
    });
    this.partService.getRequestedParts().subscribe(requested_parts_response => this.requested_parts_source.load(requested_parts_response));
    this.partService.getOrderedParts().subscribe(ordered_parts_response => this.ordered_parts_source.load(ordered_parts_response));
    this.partService.getReceivedParts().subscribe(received_parts_response => this.received_parts_source.load(received_parts_response));
  }


  public onRequestedPartClick(event) {
    this.modalsService.part = event.data;
    this.modalService.open(OrderPartModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  public onReceivedPartClick(event) {
    this.modalsService.part = event.data;
    this.modalService.open(AssignPartModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  public orderPart() {
    this.modalService.open(OrderPartModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  public onEditConfirm(event) {
    this.modalsService.receivePart(event.newData).subscribe(
      () => {
        this.ordered_parts_source.refresh();
        this.received_parts_source.refresh();
      },
      () => {
        event.confirm.reject();
      });
  }

}
