import {Component} from '@angular/core';

@Component({
  selector: 'ngx-home-dashboard',
  templateUrl: './home-dashboard.component.html',
})
export class HomeDashboardComponent {
  public role = localStorage.getItem('user_role');

  constructor() {
  }

}
