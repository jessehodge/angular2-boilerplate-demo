// Angular2 Library
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Global Services
import {SettingsService} from '../../settings/settings.service';
import {Prefs} from '../../../models/preferences/prefs';
import {LocalDataSource} from 'ng2-smart-table';
import {WorkOrderService} from '../../work-orders/work-order.service';

@Component({
  selector: 'ngx-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
})

export class AdminDashboardComponent implements OnInit {
  public prefs = new Prefs();
  public id = localStorage.getItem('user_id');
  public role = localStorage.getItem('user_role');
  public log_workorders_source = new LocalDataSource();
  public all_workorder_source = new LocalDataSource();
  public new_workorder_source = new LocalDataSource();
  public old_workorder_source = new LocalDataSource();

  all_open_work_order_settings = {
    pager: {perPage: 20}, actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'ID'},
      unit: {
        title: 'Unit #:',
        valuePrepareFunction: (unit) => {
          if (unit !== null) {
            return unit.number;
          }
        },
      },
      workorder_status: {
        title: 'Status',
        valuePrepareFunction: (status) => {
          if (status !== null) {
            return status.status;
          }
        },
      },
      deficient_properties: {
        title: 'Deficiencies',
        valuePrepareFunction: (def) => {
          if (def === '') {
            return 'Not Listed';
          } else {
            return def;
          }
        },
      },
    },
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute, public settingService: SettingsService,
              private workOrderService: WorkOrderService) {
  }

  ngOnInit() {
    this.settingService.getPrefs(this.id).subscribe(res => this.prefs = res);
    this.workOrderService.getAllOpenWorkOrders()
      .subscribe(open_work_order_response => {
        this.all_workorder_source.load(open_work_order_response.all);
        this.old_workorder_source.load(open_work_order_response.old);
        this.new_workorder_source.load(open_work_order_response.new);
      });
    this.workOrderService.getLogWorkOrders().subscribe(log_workorder_response => this.log_workorders_source.load(log_workorder_response));
  }

  public onUnitClick(event) {
    this.router.navigate(['/pages/units/unit_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  public onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

}
