// Modules
import {NgModule} from '@angular/core';
import {AngularEchartsModule} from 'ngx-echarts';
import {ThemeModule} from '../../@theme/theme.module';
import {DashboardComponent} from './dashboard.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {RouterModule} from '@angular/router';
// Components
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {HomeDashboardComponent} from './home-dashboard/home-dashboard.component';
// Model Services
import {WorkOrderService} from '../work-orders/work-order.service';
import {UnitService} from '../units/units.service';
import {UsersService} from '../users/users.service';
import {PartDashboardComponent} from './part-dashboard/part-dashboard.component';
import {ModalService} from '../modals/modals.service';
import {PartService} from '../parts/parts.service';
import {SettingsService} from '../settings/settings.service';


// Services

@NgModule({
  imports: [
    ThemeModule,
    AngularEchartsModule,
    Ng2SmartTableModule,
    RouterModule,
  ],
  declarations: [
    DashboardComponent,
    AdminDashboardComponent,
    HomeDashboardComponent,
    PartDashboardComponent,
  ],
  providers: [
    WorkOrderService,
    UnitService,
    UsersService,
    ModalService,
    PartService,
    SettingsService,
  ],
})
export class DashboardModule {
}
