import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';
import {AbstractControl} from '@angular/forms';

@Injectable()
export class JobsService {
  private _jobs_url = this.data.base_url + '/jobs/';
  private _job_url = this.data.base_url + '/job/';
  private _job_status_url = this.data.base_url + '/job_statuses/';
  public formMessage: string;

  constructor(private data: DataService) {
  }

  createJob(job) {
    const url = this._job_url;
    return this.data.post(url, job);
  }

  getJobs() {
    const url = this._jobs_url;
    return this.data.get(url);
  }

  getJob(id) {
    const url = this._job_url + id + '/';
    return this.data.get(url);
  }

  updateJob(job) {
    const url = this._job_url + job.id + '/';
    return this.data.put(url, job);
  }

  getJobStatuses() {
    const url = this._job_status_url;
    return this.data.get(url);
  }

  setMessage(c: AbstractControl, key) {
    this.formMessage = '';
    if ((c.touched || c.dirty) && c.errors) {
      const errors = Object.keys(c.errors);
      if (errors[0] === 'minlength') {
        errors.map(() => this.validationMinMessages(key));
      } else if (errors[0] === 'required') {
        errors.map(() => this.validationRequiredMessage(key));
      }
    }
  }

  private validationMinMessages(controlKey) {
    this.formMessage = controlKey + ' must be at least 4 characters in length';
  }
  private validationRequiredMessage(controlKey) {
    this.formMessage = controlKey + ' is required';
  }
}
