import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router} from '@angular/router';
import {YardService} from '../../yards/yards.service';
import {JobsService} from '../jobs.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounce';

@Component({
  selector: 'ngx-job-creation',
  templateUrl: './job-creation.component.html',
})
export class JobCreationComponent implements OnInit, OnDestroy {
  public jobForm: FormGroup;
  public formMessage: string;
  public formControl: string;
  public job_statuses = [];
  public yards = [];
  public ngUnsubscribe = new Subject();

  constructor(private jobService: JobsService,
              private router: Router,
              private yardService: YardService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.yardService.getYards()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(yard_res => this.yards = yard_res.yards);
    this.jobService.getJobStatuses()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(job_status_res => this.job_statuses = job_status_res);
    this.jobForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      number: ['', [Validators.required, Validators.minLength(4)]],
      job_status_id: ['', [Validators.required]],
      yard_id: ['', [Validators.required]],
    });

    Object.keys(this.jobForm.controls).forEach(key => {
      this.jobForm.get(key).valueChanges.debounceTime(1000).subscribe(() => {
        this.jobService.setMessage(this.jobForm.controls[key], key);
        this.formMessage = this.jobService.formMessage;
        this.formControl = key;
      });
    });
  }


  submitJob(job) {
    this.jobService.createJob(job.value).subscribe(() => this.router.navigate(['pages/jobs/']));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
