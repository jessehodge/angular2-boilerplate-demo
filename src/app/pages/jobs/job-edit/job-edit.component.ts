import {Component, OnInit} from '@angular/core';
import {Jobs} from '../../../models/jobs/jobs';
import {ActivatedRoute, Router} from '@angular/router';
import {JobsService} from '../jobs.service';
import {YardService} from '../../yards/yards.service';

@Component({
  selector: 'ngx-job-edit',
  templateUrl: './job-edit.component.html',
})
export class JobEditComponent implements OnInit {
  public job = new Jobs();
  public job_statuses = [];
  public yards = [];
  private id: number;


  constructor(private activatedRoute: ActivatedRoute, private jobService: JobsService, private router: Router,
              private yardService: YardService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.jobService.getJob(this.id).subscribe(res => this.job = res);
    this.yardService.getYards().subscribe(yard_res => this.yards = yard_res.yards);
    this.jobService.getJobStatuses().subscribe(job_status_res => this.job_statuses = job_status_res);
  }

  submitJob(job) {
    this.jobService.updateJob(job).subscribe(() => this.router.navigate(['pages/jobs/job_detail', job.id]));
  }

}
