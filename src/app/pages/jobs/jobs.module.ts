import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JobsComponent} from './jobs.component';
import {JobCreationComponent} from './job-creation/job-creation.component';
import {JobEditComponent} from './job-edit/job-edit.component';
import {JobDetailComponent} from './job-detail/job-detail.component';
import {JobTableComponent} from './job-table/job-table.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../../@theme/theme.module';
import {ReactiveFormsModule} from '@angular/forms';
import {JobRouting} from './jobs.routes';
import {JobsService} from './jobs.service';
import {YardService} from '../yards/yards.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    ThemeModule,
    JobRouting,
  ],
  declarations: [JobsComponent, JobCreationComponent, JobEditComponent, JobDetailComponent, JobTableComponent],
  providers: [JobsService, YardService],
})
export class JobsModule {
}
