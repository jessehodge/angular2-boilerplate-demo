import {RouterModule, Routes} from '@angular/router';
import {VendorsComponent} from './vendors.component';
import {VendorCreationComponent} from './vendor-creation/vendor-creation.component';
import {VendorDetailComponent} from './vendor-detail/vendor-detail.component';
import {VendorEditComponent} from './vendor-edit/vendor-edit.component';
import {VendorTableComponent} from './vendor-table/vendor-table.component';

const routes: Routes = [
  {
    path: '',
    component: VendorsComponent, children: [
      {path: '', component: VendorTableComponent},
      {path: 'vendor_detail/:id', component: VendorDetailComponent},
      {path: 'vendor_creation', component: VendorCreationComponent},
      {path: 'vendor_edit/:id', component: VendorEditComponent},
    ],
  }];

export const VendorRouting = RouterModule.forChild(routes);
