import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VendorService} from '../vendors.service';
import {Vendor} from '../../../models/vendors/vendor';
import {Parts} from '../../../models/parts/parts';
import {LocalDataSource} from 'ng2-smart-table';
import {PartService} from '../../parts/parts.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-vendor-detail',
  templateUrl: './vendor-detail.component.html',
})

export class VendorDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public vendor = new Vendor();
  public parts = new Array<Parts>();
  public source: LocalDataSource = new LocalDataSource();
  settings = {
    pager: {perPage: 20},
    actions: {add: true, edit: true, delete: false},
    add: {
      addButtonContent: '<i class="nb-plus"></i>', createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>', confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>', saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>', confirmSave: true,
    },
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      total_cost: {title: 'Cost'}, qty: {title: 'Qty'},
    },
  };
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private vendorService: VendorService,
              private activatedRoute: ActivatedRoute,
              private partService: PartService,
              private router: Router) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.vendorService.getVendor(this.id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          this.source.load(res.parts);
          this.vendor = res;
        });
  }

  editVendor() {
    this.router.navigate(['/pages/vendors/vendor_edit/', this.id], {relativeTo: this.activatedRoute});
  }

  onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  onCreateConfirm(event) {
    event.newData['vendor_id'] = this.id;
    this.partService.postPart(event.newData)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          alert('Part has been created!');
          event.confirm.resolve();
        },
        err => {
          event.confirm.reject();
        });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
