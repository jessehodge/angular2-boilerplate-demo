import {Component, OnInit} from '@angular/core';
import {Prefs} from '../../../models/preferences/prefs';
import {SettingsService} from '../settings.service';
import {PartService} from '../../parts/parts.service';
import {PartCount} from '../../../models/parts/part-count';

@Component({
  selector: 'ngx-part-settings',
  templateUrl: './part-settings.component.html',
})
export class PartSettingsComponent implements OnInit {
  public prefs = new Prefs();
  public id = +localStorage.getItem('user_id');
  public part_count = new PartCount();

  constructor(public settingService: SettingsService,
              private partService: PartService) {
  }

  ngOnInit() {
    const id = localStorage.getItem('user_id');
    this.settingService.getPrefs(id).subscribe(res => {
      this.prefs = res;
    });
    this.partService.getPartCount().subscribe(part_count_response => {
      this.part_count = part_count_response;
    });
  }

  saveSettings(prefs) {
    prefs.id = this.id;
    this.settingService.saveSettings(prefs).subscribe();
  }

}
