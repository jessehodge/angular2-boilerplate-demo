import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class SettingsService {

  public company: any;
  public unit: any;
  public user: any;
  private _company_url = this.dataService.base_url + '/company/';
  private _companies_url = this.dataService.base_url + '/companies/';
  private _prefs_url = this.dataService.base_url + '/user_preference/';

  constructor(private dataService: DataService) {
  }

  createCompany(company) {
    const url = this._company_url;
    return this.dataService.post(url, company);
  }

  getCompanies() {
    const url = this._companies_url;
    return this.dataService.get(url);
  }

  getPrefs(id) {
    const url = this._prefs_url + id + '/';
    return this.dataService.get(url);
  }

  saveSettings(prefs) {
    const url = this._prefs_url + prefs.id + '/';
    return this.dataService.put(url, prefs);
  }

}
