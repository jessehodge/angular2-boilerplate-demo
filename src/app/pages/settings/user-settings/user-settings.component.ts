import {Component, OnInit} from '@angular/core';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalService} from '../../modals/modals.service';
import {SettingsService} from '../settings.service';

import {UserCreateModalComponent} from '../../modals/users/user-creation/user-creation.modal';
import {UserEditModalComponent} from '../../modals/users/user-edit/user-edit.modal';
import {LocalDataSource} from 'ng2-smart-table';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-user-settings',
  templateUrl: './user-settings.component.html',
})
export class UserSettingsComponent implements OnInit {
  public users_source = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {name: {title: 'Name'}, email: {title: 'Email'}},
  };

  constructor(private settingsService: SettingsService,
              private modalService: NgbModal,
              private localModalService: ModalService,
              private userService: UsersService) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(user_response => this.users_source.load(user_response.users));
  }

  createUser() {
    this.modalService.open(UserCreateModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  editUser(event) {
    this.localModalService.user = event.data;
    this.modalService.open(UserEditModalComponent, {size: 'lg', container: 'nb-layout'});
  }
}
