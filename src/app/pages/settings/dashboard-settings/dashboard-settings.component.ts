import {Component, OnInit} from '@angular/core';
import {Prefs} from '../../../models/preferences/prefs';
import {SettingsService} from '../settings.service';
import {OpenWorkOrder} from '../../../models/work-orders/open-work-order';
import {WorkOrderService} from '../../work-orders/work-order.service';

@Component({
  selector: 'ngx-dashboard-settings',
  templateUrl: './dashboard-settings.component.html',
})
export class DashboardSettingsComponent implements OnInit {
  public prefs = new Prefs();
  public admin_open_workorders = new OpenWorkOrder();
  public id = +localStorage.getItem('user_id');

  constructor(public settingService: SettingsService,
              private workOrderService: WorkOrderService) {
  }

  ngOnInit() {
    this.settingService.getPrefs(this.id).subscribe(res => this.prefs = res);
    this.workOrderService.getOpenWorkOrderCount()
      .subscribe(workorder_count_response => this.admin_open_workorders = workorder_count_response);
  }

  saveSettings(prefs) {
    prefs.id = this.id;
    this.settingService.saveSettings(prefs).subscribe();
  }

}
