import {Component, OnInit} from '@angular/core';

import {Companies} from '../../../models/companies/companies';

import {SettingsService} from '../settings.service';
import {LocalDataSource} from 'ng2-smart-table';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {EditCompanyModalComponent} from '../../modals/company/company-edit/company-edit.modal';

@Component({
  selector: 'ngx-company-settings',
  templateUrl: './company-settings.component.html',
})
export class CompanySettingsComponent implements OnInit {
  public company = new Companies();
  public companies = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {name: {title: 'Name'}},
  };

  constructor(private settingsService: SettingsService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.settingsService.getCompanies().subscribe(res => this.source.load(res));
  }

  createCompany(company) {
    this.settingsService.createCompany(company).subscribe(() => {
      this.source.refresh();
    });
  }

  public onCompanyClick(event) {
    const contentComponentInstance = this.modalService.open(EditCompanyModalComponent, {
      size: 'lg', container: 'nb-layout',
    }).componentInstance;
    contentComponentInstance.company = event.data;
  }

}
