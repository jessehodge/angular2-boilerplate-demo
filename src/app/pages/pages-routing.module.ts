import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PagesComponent} from './pages.component';
import {HomeDashboardComponent} from './dashboard/home-dashboard/home-dashboard.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from '../guards/auth-guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: HomeDashboardComponent,
      canActivate: [AuthGuard],
    }, {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    }, {
      path: 'login',
      component: LoginComponent,
    }, {
      path: 'users',
      loadChildren: './users/users.module#UsersModule',
      canActivate: [AuthGuard],
    }, {
      path: 'locations',
      loadChildren: './locations/locations.module#LocationsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'units',
      loadChildren: './units/units.module#UnitsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'logs',
      loadChildren: './logs/logs.module#LogsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'messages',
      loadChildren: './messages/messages.module#MessageModule',
      canActivate: [AuthGuard],
    }, {
      path: 'work_orders',
      loadChildren: './work-orders/work-order.module#WorkOrderModule',
      canActivate: [AuthGuard],
    }, {
      path: 'vendors',
      loadChildren: './vendors/vendors.module#VendorsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'jobs',
      loadChildren: './jobs/jobs.module#JobsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'parts',
      loadChildren: './parts/parts.module#PartsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'yards',
      loadChildren: './yards/yards.module#YardsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'locations',
      loadChildren: './locations/locations.module#LocationsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule',
      canActivate: [AuthGuard],
    }, {
      path: 'reports',
      loadChildren: './reports/reports.module#ReportsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'settings',
      loadChildren: './settings/settings.module#SettingsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'timeclock',
      loadChildren: './timeclock/timeclock.module#TimeclockModule',
      canActivate: [AuthGuard],
    }, {
      path: 'mail',
      loadChildren: './mail/mail.module#MailModule',
      canActivate: [AuthGuard],
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
