import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ReportsComponent} from './reports.component';
import {ReportsRouting} from './reports.routes';
import {WeeklyReportsComponent} from './weekly-reports/weekly-reports.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../../@theme/theme.module';
import {ReportsService} from './reports.service';

@NgModule({
  imports: [
    CommonModule,
    ReportsRouting,
    ThemeModule,
    Ng2SmartTableModule,
  ],
  declarations: [ReportsComponent, WeeklyReportsComponent],
  providers: [ReportsService, DatePipe],
})
export class ReportsModule {
}
