import {Component, OnInit} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Angular2Csv} from 'angular2-csv';
import {WorkOrder} from '../../../models/work-orders/work-order';
import {Vendor} from '../../../models/vendors/vendor';
import {ReportsService} from '../reports.service';
import {DatePipe} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-weekly-reports',
  templateUrl: './weekly-reports.component.html',
})
export class WeeklyReportsComponent implements OnInit {

  public mechanic_source: LocalDataSource = new LocalDataSource();
  public vendor_source: LocalDataSource = new LocalDataSource();
  public mechanic_work = new Array<WorkOrder>();
  public vendor_work = new Array<Vendor>();
  mech_header_array = [
    {
      id: 'Id',
      date: 'Date',
      location: 'Location',
      unit_number: 'Unit #',
      unit_description: 'Unit Desc',
      repair_description: 'Repair Desc',
      mechanic: 'Mechanic',
      status: 'Status',
      unit_type: 'Type',
      meter: 'Meter',
    }];
  vendor_header_array = [
    {
      id: 'Work Order #:',
      date: 'Date',
      meter: 'Meter',
      unit: 'Unit Number',
      unit_description: 'Unit Desc',
      repair_description: 'Repair Desc',
      unit_status: 'Unit Status',
      type: 'Type',
      vendor: 'Vendor Name',
    }];
  mech_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Work Order #:'},
      created_at: {
        title: 'Date',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      unit_number: {title: 'Unit Number'},
      type: {title: 'Type'},
      unit_description: {title: 'Unit Desc'},
      other_description: {title: 'Repair Desc'},
      mechanic: {title: 'Mechanic'},
      location: {title: 'Location'},
      unit_status: {title: 'Status'},
      meter: {title: 'Meter'},
    },
  };
  vendor_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Work Order #:'},
      created_at: {
        title: 'Date', valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      unit_number: {title: 'Unit Number'}, type: {title: 'Type'},
      unit_status: {title: 'Unit Status'}, unit_description: {title: 'Unit Desc'},
      other_description: {title: 'Repair Desc'}, vendor: {title: 'Vendor Name'},
    },
  };

  constructor(private reportService: ReportsService,
              private datePipe: DatePipe,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.reportService.getWeeklyReports().subscribe(
      res => {
        this.mechanic_work = res.ours;
        this.vendor_work = res.vendor;
        this.mechanic_source.load(res.ours);
        this.vendor_source.load(res.vendor);
      });
  }

  public createMechCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
    };
    const new_array = [...this.mech_header_array, ...this.mechanic_work];
    const create_csv = new Angular2Csv(new_array, 'Work Order Report', options);
  }

  createVendorCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
    };
    const new_array = [...this.vendor_header_array, ...this.vendor_work];
    const create_csv = new Angular2Csv(new_array, 'Work Order Report', options);
  }

  public onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

}
