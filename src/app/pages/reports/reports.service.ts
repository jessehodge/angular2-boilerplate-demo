import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class ReportsService {
  public unit_number: number;
  public work_order_detail_object: any;
  private _work_order_reports_url = this.data.base_url + '/workorders/weekly_report/';

  constructor(private data: DataService) {
  }

  public getWeeklyReports() {
    const url = this._work_order_reports_url;
    return this.data.get(url);
  }


}
