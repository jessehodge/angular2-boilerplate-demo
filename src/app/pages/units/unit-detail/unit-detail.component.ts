import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UnitService} from '../units.service';
import {Units} from '../../../models/units/units';
import {LocalDataSource} from 'ng2-smart-table';
import {PartService} from '../../parts/parts.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {DatePipe} from '@angular/common';
import {JobsService} from '../../jobs/jobs.service';
import {Jobs} from '../../../models/jobs/jobs';
import {OrderUnitPartModalComponent} from '../../modals/parts/order-unit-part/order-unit-part.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngx-unit-detail',
  templateUrl: './unit-detail.component.html',
})

export class UnitDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public job = new Jobs();
  public unit = new Units();
  public logs: LocalDataSource = new LocalDataSource();
  public work_orders: LocalDataSource = new LocalDataSource();
  public parts: LocalDataSource = new LocalDataSource();
  public activity_logs: LocalDataSource = new LocalDataSource();
  public jobs = [];
  log_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      deficiency: {title: 'Deficiencies'},
      job: {
        title: 'Job',
        valuePrepareFunction: (job) => {
          if (job) {
            return job.name;
          }
        },
      },
    },
  };
  wo_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'},
      created_at: {
        title: 'Date Created', valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      workorder_status: {title: 'Status', valuePrepareFunction: (status) => status.status},
      other_description: {title: 'Brief Description'},
    },
  };
  parts_settings = {
    pager: {perPage: 20},
    actions: {add: false, edit: false, delete: false},
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      total_cost: {title: 'Cost'},
      qty: {title: 'Qty'},
    },
  };
  activity_settings = {
    pager: {perPage: 10},
    actions: {add: false, edit: false, delete: false},
    columns: {
      created_at: {
        title: 'Date Created', valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      }, message: {title: 'Message'},
    },
  };
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private unitService: UnitService, private activatedRoute: ActivatedRoute, private partService: PartService,
              private router: Router, private datePipe: DatePipe, private jobService: JobsService, private modalService: NgbModal) {
    this.activatedRoute.params.takeUntil(this.ngUnsubscribe).subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.unitService.getUnitById(this.id).takeUntil(this.ngUnsubscribe).subscribe(unit_res => {
      this.unit = unit_res;
      this.logs.load(unit_res.logs);
      this.activity_logs.load(unit_res.unit_activity_logs);
      this.work_orders.load(unit_res.workorders);
      this.parts.load(unit_res.parts);
    });
    this.jobService.getJobs().takeUntil(this.ngUnsubscribe).subscribe(job_res => this.jobs = job_res);
  }

  onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id]);
  }

  onLogClick(event) {
    this.router.navigate(['/pages/logs/logs_detail/', event.data.id]);
  }

  onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  updateUnit(unit) {
    this.job = unit.job_name;
    unit.job_name = this.job.name;
    unit.job_id = this.job.id;
    this.unitService.updateUnit(unit);
  }

  addPartOrderedAndInstalled() {
    const contentComponentInstance = this.modalService.open(OrderUnitPartModalComponent,
      {size: 'lg', container: 'nb-layout'}).componentInstance;
    contentComponentInstance.part.unit_id = this.unit.id;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
