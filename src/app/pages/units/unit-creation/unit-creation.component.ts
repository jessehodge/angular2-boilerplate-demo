import {Component, OnDestroy, OnInit} from '@angular/core';
import {Units} from '../../../models/units/units';
import {UnitService} from '../units.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {UnitTypeModalComponent} from '../../modals/units/unit-type/unit-type.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UnitClassModalComponent} from '../../modals/units/unit-class/unit-class.modal';
import {CompanyCreationModalComponent} from '../../modals/company/company-creation/company-creation.modal';
import {UnitStatusModalComponent} from '../../modals/units/unit-status/unit-status.modal';
import {UserCreateModalComponent} from '../../modals/users/user-creation/user-creation.modal';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-unit-creation',
  templateUrl: './unit-creation.component.html',
})

export class UnitCreationComponent implements OnInit, OnDestroy {
  public unit = new Units();
  public operators = [];
  public unitStatuses: any;
  public unitTypes: any;
  public unit_statuses;
  public unitSubTypes: any;
  public companies: any;
  public id: number;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private unitService: UnitService, private router: Router, private modalService: NgbModal,
              private operatorService: UsersService) {
  }

  ngOnInit() {
    this.unitService.getUnitStatuses().takeUntil(this.ngUnsubscribe).subscribe(res => this.unitStatuses = res);
    this.unitService.getUnitTypes().takeUntil(this.ngUnsubscribe).subscribe(res => this.unitTypes = res);
    this.unitService.getCompanies().takeUntil(this.ngUnsubscribe).subscribe(res => this.companies = res);
  }

  getOperators() {
    this.operatorService.getOperators().takeUntil(this.ngUnsubscribe).subscribe(res => this.operators = res.users);
  }

  getUnitSubTypes(utype) {
    this.unitService.getUnitTypeSubTypes(+utype.unit_type_id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => this.unitSubTypes = res.unit_subtypes);
  }

  openUnitTypeModal() {
    this.modalService.open(UnitTypeModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  openUnitSubTypeModal() {
    this.modalService.open(UnitClassModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  openUnitStatusModal() {
    this.modalService.open(UnitStatusModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  openCompanyModal() {
    this.modalService.open(CompanyCreationModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  createOperatorModal() {
    this.modalService.open(UserCreateModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  valueFormatter(data: any) {
    return data.name;
  }

  submitUnit(unit) {
    this.unitService.doubleZeroUnitNumber(unit.number);
    this.unitService.postUnit(unit).takeUntil(this.ngUnsubscribe)
      .subscribe(res => this.router.navigate(['/pages/units/unit_detail/', res.id]));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
