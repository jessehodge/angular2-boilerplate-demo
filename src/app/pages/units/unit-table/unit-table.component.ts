import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalDataSource} from 'ng2-smart-table';
import {UnitService} from '../units.service';

@Component({
  selector: 'ngx-unit-table',
  templateUrl: './unit-table.component.html',
  styles: [`nb-card { transform: translate3d(0, 0, 0); }`],
})
export class UnitTableComponent implements OnInit {
  public id: number;
  public unit_source = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      number: {title: 'Unit #:'}, unit_type: {title: 'Unit Type'}, vin_sn: {title: 'Vin-S/N'},
      description: {title: 'Description'},
    },
  };

  constructor(private _router: Router,
              private activatedRoute: ActivatedRoute,
              private unitService: UnitService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.unitService.getUnits().subscribe(unit_response => {
      this.unit_source.load(unit_response);
    });
  }

  public onUnitClick(event) {
    this._router.navigate(['unit_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

}
