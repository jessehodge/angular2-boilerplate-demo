import {Component} from '@angular/core';

@Component({
  selector: 'ngx-units',
  template: `
    <router-outlet></router-outlet>`,
  styles: ['units.component.css'],
})
export class UnitsComponent {
  constructor() {
  }
}
