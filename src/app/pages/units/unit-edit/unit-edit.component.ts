import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UnitService} from '../units.service';
import {Units} from '../../../models/units/units';
import {UnitClassModalComponent} from '../../modals/units/unit-class/unit-class.modal';
import {UnitTypeModalComponent} from '../../modals/units/unit-type/unit-type.modal';
import {CompanyCreationModalComponent} from '../../modals/company/company-creation/company-creation.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {JobsService} from '../../jobs/jobs.service';

@Component({
  selector: 'ngx-unit-edit',
  templateUrl: './unit-edit.component.html',
})

export class UnitEditComponent implements OnInit {
  public unit = new Units();
  public jobs: any;
  public unitStatuses: any;
  public unitTypes: any;
  public unitSubTypes: any;
  public companies: any;
  public id: number;

  constructor(private unitService: UnitService, private router: Router, private activatedRoute: ActivatedRoute,
              private modalService: NgbModal, private jobService: JobsService) {
    this.activatedRoute.params.subscribe(p => {
      const id: number = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.unitService.getUnitById(this.id).subscribe(res => this.unit = res);
    this.unitService.getUnitStatuses().subscribe(res => this.unitStatuses = res);
    this.unitService.getUnitTypes().subscribe(res => this.unitTypes = res);
    this.unitService.getUnitSubTypes().subscribe(res => this.unitSubTypes = res);
    this.unitService.getCompanies().subscribe(res => this.companies = res);
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
  }

  getUnitSubTypes() {
    this.unitService.getUnitTypeSubTypes(this.id).subscribe(res => this.unitSubTypes = res.sub_types);
  }

  submitUnit(unit) {
    this.unitService.updateUnit(unit).subscribe(() => this.router.navigate(['pages/units/unit_detail/', this.id]));
  }

  createClass() {
    this.modalService.open(UnitClassModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  createUnitType() {
    this.modalService.open(UnitTypeModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  createCompany() {
    this.modalService.open(CompanyCreationModalComponent, {size: 'lg', container: 'nb-layout'});
  }
}
