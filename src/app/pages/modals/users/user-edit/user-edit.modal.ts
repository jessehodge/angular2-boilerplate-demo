// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {User} from '../../../../models/user/user';
import {JobsService} from '../../../jobs/jobs.service';

@Component({
  selector: 'ngx-user-edit-modal',
  templateUrl: './user-edit.modal.html',
})
export class UserEditModalComponent implements OnInit {
  // Public Model Objects
  public user = new User();
  public roles = [];
  public users = [];
  public jobs = [];
  public selected_roles = [];
  public dropdownList = [];
  public dropDownSettings = {};

  constructor(public activeModal: NgbActiveModal, private modalService: ModalService, private jobService: JobsService) {
  }

  ngOnInit() {
    this.dropDownSettings = {
      singleSelection: false,
      text: 'Select Roles',
      enableSearchFilter: false,
      enableCheckAll: false,
      classes: 'myclass custom-class',
      badgeShowLimit: null,
    };
    this.user = this.modalService.user;
    this.modalService.getRoles().subscribe(res => this.dropdownList = res);
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  onItemSelect(item: any) {
    if (this.user.roles.filter(x => x.id === item.id) === false) {
      this.user.roles.push(item);
    }
  }

  OnItemDeSelect(item: any) {
    if (this.user.roles.includes(item) === true) {
      const index = this.user.roles.indexOf(item, 0);
      if (index > -1) {
        this.user.roles.splice(index, 1);
      }
    }
  }

  editUser(user) {
    this.user.job_id = user.job.id;
    this.user.job = user.job.name;
    this.user.roles.map(x => this.selected_roles.push(x.id));
    this.user.roles = Array.from(new Set(this.selected_roles));
    this.modalService.editUser(this.user).subscribe();
    this.activeModal.close();
  }
}
