// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {User} from '../../../../models/user/user';
import {JobsService} from '../../../jobs/jobs.service';

@Component({
  selector: 'ngx-user-creation-modal',
  templateUrl: './user-creation.modal.html',
})
export class UserCreateModalComponent implements OnInit {
  // Public Model Objects
  public user = new User();
  public users = [];
  public jobs = [];
  public roles = [];
  public dropdownList = [];
  public selected_roles = [];
  public dropDownSettings = {};

  constructor(private activeModal: NgbActiveModal,
              private modalService: ModalService,
              private jobService: JobsService) {
  }

  ngOnInit() {
    this.dropDownSettings = {
      singleSelection: false,
      text: 'Select Roles',
      enableSearchFilter: false,
      enableCheckAll: false,
      classes: 'myclass custom-class',
      badgeShowLimit: null,
    };
    this.modalService.getRoles().subscribe(res => this.dropdownList = res);
    this.jobService.getJobs().subscribe(job_res => this.jobs = job_res.jobs);
  }

  onItemSelect(item: any) {
    if (this.user.roles.filter(x => x.id === item.id) === false) {
      this.user.roles.push(item);
    }
  }

  OnItemDeSelect(item: any) {
    if (this.user.roles.includes(item) === true) {
      const index = this.user.roles.indexOf(item, 0);
      if (index > -1) {
        this.user.roles.splice(index, 1);
      }
    }
  }

  closeModal() {
    this.activeModal.close();
  }

  createUser(user) {
    user.roles.map(x => this.selected_roles.push(x.id));
    user.roles = Array.from(new Set(this.selected_roles));
    this.modalService.registerUser(user).subscribe();
    // this.closeModal();
  }
}
