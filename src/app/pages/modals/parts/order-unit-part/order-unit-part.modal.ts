// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Party Libraries
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {Parts} from '../../../../models/parts/parts';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {Units} from '../../../../models/units/units';
import {VendorService} from '../../../vendors/vendors.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './order-unit-part.modal.html',
})

export class OrderUnitPartModalComponent implements OnInit {
  // Object Variables
  public part = new Parts();
  public work_order = new WorkOrder();
  public unit = new Units();
  public vendors = [];

  // Public Variables
  public quantity: number;
  public id: any;

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private vendorService: VendorService) {
  }

  ngOnInit() {
    if (this.modalService.part) {
      this.part = this.modalService.part;
    }
    this.vendorService.getVendors().subscribe(
      res => {
        this.vendors = res;
      });
  }

  closeModal() {
    this.activeModal.close();
  }

  vendorValueFormatter(data: any): string {
    return data.name;
  }

  orderPart(part) {
    this.modalService.orderPart(part);
    this.closeModal();
  }
}
