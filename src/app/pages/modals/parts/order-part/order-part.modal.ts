// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Party Libraries
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {WorkOrderService} from '../../../work-orders/work-order.service';
// Models
import {Parts} from '../../../../models/parts/parts';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {Units} from '../../../../models/units/units';
import {VendorService} from '../../../vendors/vendors.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './order-part.modal.html',
})

export class OrderPartModalComponent implements OnInit {
  // Object Variables
  public part = new Parts();
  public work_order = new WorkOrder();
  public unit = new Units();
  public vendors = [];

  // Public Variables
  public quantity: number;
  public id: any;

  constructor(private activeModal: NgbActiveModal,
              private modalService: ModalService,
              private workOrderService: WorkOrderService,
              private vendorService: VendorService) {
  }

  ngOnInit() {
    if (this.modalService.part) {
      this.part = this.modalService.part;
    }
    this.vendorService.getVendors().subscribe(res => this.vendors = res);
  }

  closeModal() {
    this.activeModal.close();
  }

  vendorValueFormatter(data: any): string {
    return data.name;
  }

  orderPart(part) {
    if (this.work_order) {
      part.workorder_id = this.work_order.id;
    }
    if (this.unit) {
      part.unit = this.unit.id;
    }
    part.vendor_id = part.vendor_id.id;
    this.modalService.orderPart(part).subscribe();
    this.closeModal();
  }
}
