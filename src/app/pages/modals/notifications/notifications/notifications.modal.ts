import {Component, OnInit} from '@angular/core';

import {Notifications} from '../../../../models/notifications/notifications';
import {ApiService} from '../../../../services/api-service';
import {Router} from '@angular/router';
import {LocalDataSource} from 'ng2-smart-table';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-event-modal',
  templateUrl: './notifications.modal.html',
})

export class NotificationsModalComponent implements OnInit {

  public user_id = localStorage.getItem('user_id');
  public notification_source = new LocalDataSource();
  notification_settings = {
    pager: {perPage: 20}, actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {message: {title: 'Notifications'}},
  };

  constructor(private apiService: ApiService,
              private router: Router,
              private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    this.apiService.getNotifications(this.user_id)
      .subscribe(not_res => {
        if (not_res) {
          this.notification_source.load(not_res);
        }
      });
  }

  navigateSequence(notification) {
    this.apiService.updateNotification(notification);
    this.activeModal.close();
  }

  navigateToNotification(event) {
    const notification = event.data;
    if (notification.workorder_id) {
      this.navigateSequence(notification);
      this.router.navigate(['/pages/work_orders/work_order_detail/', notification.workorder_id]);
    } else if (notification.vendor_id) {
      this.navigateSequence(notification);
      this.router.navigate(['/pages/vendors/vendor_detail/', notification.vendor_id]);
    } else if (notification.part_id) {
      this.navigateSequence(notification);
      this.router.navigate(['/pages/parts/part_detail/', notification.part_id]);
    } else if (notification.ordered_part_id) {
      this.navigateSequence(notification);
      this.router.navigate(['/pages/parts/part_detail/', notification.ordered_part_id]);
    }
  }
}
