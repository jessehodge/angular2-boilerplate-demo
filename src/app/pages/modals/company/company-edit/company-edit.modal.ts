// Angular2 Library
import {Component} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {Companies} from '../../../../models/companies/companies';

@Component({
  selector: 'ngx-company-modal',
  templateUrl: './company-edit.modal.html',
})
export class EditCompanyModalComponent {
  // Public Model Objects
  public company = new Companies();

  // Public Variables
  public quantity: number;

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService) {
  }

  closeModal() {
    this.activeModal.close();
  }

  updateCompany(company) {
    this.modalService.createCompany(company).subscribe();
    this.closeModal();
  }
}
