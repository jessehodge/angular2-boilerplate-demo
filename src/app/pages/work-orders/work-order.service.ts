import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class WorkOrderService {
  // Variables
  public unit_number: number;
  public work_order_detail_object: any;
  // URLS
  private _all_work_orders_url = this.data.base_url + '/workorders/';
  private _work_order_url = this.data.base_url + '/workorder/';
  private _existing_work_order = this.data.base_url + '/workorders/open/';
  private _work_order_statuses = this.data.base_url + '/workorder_statuses/';
  private _call_logs_url = this.data.base_url + '/call_logs/workorder/';
  private _workorder_parts = this.data.base_url + '/parts/ordered/workorders/';
  private _vendor_workorders_url = this.data.base_url + '/workorders/vendor/';
  private _open_workorder_list_url = this.data.base_url + '/workorders/open_list/';
  private _log_work_orders = this.data.base_url + '/workorders/by_log/';
  private _workorders_count = this.data.base_url + '/workorders/open_count/';
  private _work_order_vendor = this.data.base_url + '/workorder/vendor/';
  private _work_order_mechanic = this.data.base_url + '/workorder/mechanic/';
  private _work_order_confirm_url = this.data.base_url + '/workorder/confirm/';

  constructor(private data: DataService) {
  }

  // --------------------------------------------------WORK ORDERS-----------------------------------------------//

  // GET REQUESTS
  getVendorWorkOrders() {
    const url = this._vendor_workorders_url;
    return this.data.get(url);
  }

  getOpenWorkOrderCount() {
    const url = this._workorders_count;
    return this.data.get(url);
  }

  getLogWorkOrders() {
    const url = this._log_work_orders;
    return this.data.get(url);
  }

  getAllOpenWorkOrders() {
    const url = this._open_workorder_list_url;
    return this.data.get(url);
  }

  getWorkOrder(id) {
    const url = this._work_order_url + id + '/';
    return this.data.get(url);
  }

  getWorkOrders() {
    const url = this._all_work_orders_url;
    return this.data.get(url);
  }

  getExistingWorkOrders(number) {
    const url = this._existing_work_order + number + '/';
    return this.data.get(url);
  }

  getWorkOrderStatuses() {
    const url = this._work_order_statuses;
    return this.data.get(url);
  }

  // POST
  postWorkOrder(work_order) {
    const url = this._work_order_url;
    return this.data.post(url, work_order);
  }

  // UPDATE REQUESTS
  updateWorkOrder(work_order) {
    const url = this._work_order_url + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  updateWorkOrderConfirmation(id) {
    const url = this._work_order_confirm_url + id + '/';
    return this.data.put(url);
  }

  updateWorkOrderVendor(work_order) {
    const url = this._work_order_vendor + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  updateWorkOrderMechanic(work_order) {
    const url = this._work_order_mechanic + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  getCallLog(id) {
    const url = this._call_logs_url + id + '/';
    return this.data.get(url);
  }

  getWorkOrderParts() {
    const url = this._workorder_parts;
    return this.data.get(url);
  }

}
