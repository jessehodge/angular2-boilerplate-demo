import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {UnitService} from '../../../units/units.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';

@Component({
  selector: 'ngx-unit-status',
  templateUrl: './unit-status.component.html',
})

export class UnitStatusComponent implements OnInit, OnDestroy {
  @Input() unit_status_edit;
  @Input() unit;
  public unit_statuses = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private unitService: UnitService) { }

  ngOnInit() {
    this.unitService.getUnitStatuses().takeUntil(this.ngUnsubscribe).subscribe(unit_res => this.unit_statuses = unit_res);
  }

  public editUnitStatus() {
    this.unitService.updateUnit(this.unit).subscribe(() => window.location.reload());
    alert('Unit Status has been updated!');
    this.unit_status_edit = false;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
