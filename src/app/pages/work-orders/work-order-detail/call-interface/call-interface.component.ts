import {Component, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WorkOrderService} from '../../work-order.service';
import {LocalDataSource} from 'ng2-smart-table';
import {CallLogsModalComponent} from '../../../modals/workorders/call-logs/call-logs.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalService} from '../../../modals/modals.service';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'ngx-call-interface',
  templateUrl: './call-interface.component.html',
})
export class CallInterfaceComponent {
  // public Arrays
  @Input() call_log_source;
  public work_order = new WorkOrder();
  public call_logs = new LocalDataSource();
  // public Variables
  public id: number;
  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'},
      created_by: {
        title: 'Created By',
        valuePrepareFunction: (user) => {
          if (user !== null) {
            return user.name;
          }
        },
      },
      message: {title: 'Message'},
      followup_date: {
        title: 'Follow Up Date',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
    },
  };

  constructor(private activatedRoute: ActivatedRoute, private workOrderService: WorkOrderService, private modalService: NgbModal,
              private modalsService: ModalService, private datePipe: DatePipe) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  public onCallLogClick() {
  }

  public callLogButtonClick() {
    this.modalService.open(CallLogsModalComponent, {size: 'lg', container: 'nb-layout'});
    this.modalsService.work_order = this.work_order;
  }

}
