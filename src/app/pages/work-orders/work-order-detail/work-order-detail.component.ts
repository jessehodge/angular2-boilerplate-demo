// Angular2 Library
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Services
import {WorkOrderService} from '../work-order.service';
import {UnitService} from '../../units/units.service';
import {PartService} from '../../parts/parts.service';
import {PropertyService} from '../../properties/properties.service';
import {ModalService} from '../../modals/modals.service';
// Models
import {WorkOrder} from '../../../models/work-orders/work-order';
import {Units} from '../../../models/units/units';
import {Parts} from '../../../models/parts/parts';
import {Vendor} from '../../../models/vendors/vendor';
// Third Party Libraries
import {LocalDataSource} from 'ng2-smart-table';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// Components
import {PullPartModalComponent} from '../../modals/parts/pull-part/pull-part.modal';
import {ApiService} from '../../../services/api-service';
import {VendorService} from '../../vendors/vendors.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-work-order-detail',
  templateUrl: './work-order-detail.component.html',
})

export class WorkOrderDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();
  // Model Objects
  public unit = new Units();
  public work_order = new WorkOrder();
  public part_source = new LocalDataSource();
  public parts = new Parts();
  public vendor = new Vendor();
  // Third party object reference
  public source: LocalDataSource = new LocalDataSource();
  public call_log_source = new LocalDataSource();
  public messages = [];
  public mechanics = [];
  public vendors = [];
  // Public Variables
  public part_number_desc: string;
  public edit = false;
  public unit_status_edit = false;
  public assign = false;
  public vendor_select: boolean;
  public mechanic = false;
  public message_type = 'work_order';
  public created_by_log: boolean;

  constructor(private activatedRoute: ActivatedRoute, private workOrderService: WorkOrderService, private propService: PropertyService,
              private unitService: UnitService, private partService: PartService, private router: Router, private modalService: NgbModal,
              private pullPartModal: PullPartModalComponent, private modalsService: ModalService, private apiService: ApiService,
              private vendorService: VendorService, private userService: UsersService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.workOrderService.getWorkOrder(this.id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.created_by_log = res.created_by_log;
        this.part_source.load(res.parts);
        this.call_log_source.load(res.call_logs);
        this.messages = res.messages;
        if (res.deficient_properties !== undefined) {
          this.propService.defective_properties = res.deficient_properties.split(',');
        }
        if (res.properties_fixed !== undefined) {
          this.propService.fixed_properties = res.properties_fixed.split(',');
        }
        this.workOrderService.work_order_detail_object = res;
        this.work_order = res;
        this.unit = res.unit;
        this.vendor = res.vendor;
        this.parts = res.parts;
        this.modalsService.work_order = res;
      });
  }

  public onEdit() { this.edit = true; }

  public editUS() { this.unit_status_edit = true; }

  public assignMechanic() {
    this.userService.getMechanics().takeUntil(this.ngUnsubscribe).subscribe(mech_res => this.mechanics = mech_res.users);
    this.mechanic = true;
    this.vendor_select = false;
  }

  public assignVendor() {
    this.vendorService.getVendors().takeUntil(this.ngUnsubscribe).subscribe(vendor_res => this.vendors = vendor_res);
    this.work_order.mechanic = null;
    this.vendor_select = true;
    this.mechanic = false;
  }

  public cancelAssignment() {
    this.assign = false;
    this.vendor_select = false;
    this.mechanic = false;
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  public updateWorkOrder(work_order) {
    work_order.properties_fixed = this.propService.fixed_properties.toString();
    work_order.deficient_properties = this.propService.defective_properties.toString();
    if (work_order.vendor) {
      work_order.vendor_id = work_order.vendor.id;
      this.workOrderService.updateWorkOrderVendor(work_order).subscribe(() => this.router.navigate(['/pages/work_orders/']));
    } else if (work_order.mechanic) {
      work_order.mechanic_id = work_order.mechanic.id;
      this.workOrderService.updateWorkOrderMechanic(work_order).subscribe(() => this.router.navigate(['/pages/work_orders/']));
    } else {
      this.workOrderService.updateWorkOrder(work_order).subscribe(() => this.router.navigate(['/pages/work_orders/']));
    }
  }

  public onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  public onInvPartClick(event) {
    const contentComponentInstance = this.modalService.open(PullPartModalComponent, { size: 'lg', container: 'nb-layout'})
      .componentInstance;
    contentComponentInstance.part = event.data;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
