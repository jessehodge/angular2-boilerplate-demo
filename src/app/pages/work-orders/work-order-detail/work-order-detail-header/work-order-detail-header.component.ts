import {Component, OnDestroy, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {WorkOrderService} from '../../work-order.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';

@Component({
  selector: 'ngx-work-order-detail-header',
  templateUrl: './work-order-detail-header.component.html',
})

export class WorkOrderDetailHeaderComponent implements OnDestroy, OnInit {

  @Output() onUnitStatusEdit = new EventEmitter<boolean>();
  @Output() onEdit = new EventEmitter<boolean>();
  @Input() work_order;
  @Input() unit;
  @Input() unit_status_edit;
  @Input() id;
  @Input() edit;

  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private workOrderService: WorkOrderService) { }

  ngOnInit() { }

  public confirmWorkOrder() {
    this.workOrderService.updateWorkOrderConfirmation(this.id).subscribe();
  }

  public editWO() {
    this.onEdit.emit(true);
    this.edit = true;
  }

  public editUS() {
    this.onUnitStatusEdit.emit(true);
    this.unit_status_edit = true;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
