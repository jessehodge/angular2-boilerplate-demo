import {RouterModule, Routes} from '@angular/router';
import {WorkOrderComponent} from './work-order.component';
import {WorkOrderTableComponent} from './work-order-table/work-order-table.component';
import {WorkOrderCreationComponent} from './work-order-creation/work-order-creation.component';
import {WorkOrderDetailComponent} from './work-order-detail/work-order-detail.component';

const routes: Routes = [
  {
    path: '',
    component: WorkOrderComponent, children: [
      {path: '', component: WorkOrderTableComponent},
      {path: 'work_order_creation/:unit', component: WorkOrderCreationComponent},
      {path: 'work_order_creation', component: WorkOrderCreationComponent},
      {path: 'work_order_detail/:id', component: WorkOrderDetailComponent},
    ],
  }];

export const WorkOrderRoutes = RouterModule.forChild(routes);
