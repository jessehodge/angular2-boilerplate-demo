import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-work-orders',
  template: `<router-outlet></router-outlet>`,
})
export class WorkOrderComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
