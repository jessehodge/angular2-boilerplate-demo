import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Angular2Csv} from 'angular2-csv/Angular2-csv';
import {LocalDataSource} from 'ng2-smart-table';
import {WorkOrderService} from '../work-order.service';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'ngx-work-order-table',
  styles: [`nb-card {
    transform: translate3d(0, 0, 0);
  }`],
  templateUrl: './work-order-table.component.html',
})

export class WorkOrderTableComponent implements OnInit, OnDestroy {
  public id: number;
  public email: string;
  public vendor_workorder_source = new LocalDataSource();
  public all_workorder_source = new LocalDataSource();
  public csv_workorder = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();
  public header_array = [
    {
      id: 'Id', status: 'Status', deficient_properties: 'Deficient Properties', properties_fixed: 'Properties Fixed',
      meter: 'Meter', mechanic: 'Mechanic', operator: 'Operator', po_number: 'PO Number', unit_number: 'Unit #',
      vendor: 'Vendor', created_at: 'Date Created', unit_status: 'Unit Status', wo_type: 'Work Order Type',
      other_description: 'Other Description',
    }];

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'},
      workorder_status: {title: 'Status'},
      unit_number: {title: 'Unit #'},
      unit_status: {title: 'Unit Status'},
      other_description: {title: 'Brief Description'},
    },
  };

  vendorWorkOrderSettings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'},
      workorder_status: {title: 'Status'},
      unit_number: {title: 'Unit #'},
      unit_status: {title: 'Unit Status'},
      other_description: {title: 'Brief Description'},
      vendor: {title: 'Vendor'},
    },
  };

  constructor(private workOrdersService: WorkOrderService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.email = localStorage.getItem('uid');
    this.workOrdersService.getVendorWorkOrders().takeUntil(this.ngUnsubscribe).subscribe(res => this.vendor_workorder_source.load(res));
    this.workOrdersService.getWorkOrders().takeUntil(this.ngUnsubscribe)
      .subscribe(wo_response => this.all_workorder_source.load(wo_response));
  }

  public createCSV() {
    const options = {
      fieldSeparator: ',', quoteStrings: '"', decimalseparator: '.',
      showLabels: true, showTitle: true, useBom: true,
    };
    const new_array = [...this.header_array, ...this.csv_workorder];
    const create_csv = new Angular2Csv(new_array, 'Work Order Report', options);
  }

  public onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  public export() {
    this.createCSV();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.unsubscribe();
  }

}
