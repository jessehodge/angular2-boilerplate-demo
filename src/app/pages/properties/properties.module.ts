import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {PropertyService} from './properties.service';
import {PropertyComponent} from './properties.component';
import {PropTruckCreationComponent} from './prop-truck-creation/prop-truck-creation.component';
import {PropTrailerCreationComponent} from './prop-trailer-creation/prop-trailer-creation.component';
import {PropEquipmentCreationComponent} from './prop-equipment-creation/prop-equipment-creation.component';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    PropTruckCreationComponent,
    PropTrailerCreationComponent,
    PropEquipmentCreationComponent,
    PropertyComponent,
  ],
  exports: [
    PropTruckCreationComponent,
    PropTrailerCreationComponent,
    PropEquipmentCreationComponent,
    PropertyComponent,
  ],
  providers: [
    PropertyService,
  ],
})
export class PropertiesModule {
}
