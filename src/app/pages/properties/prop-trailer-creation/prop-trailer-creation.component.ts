import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PropertyService} from '../properties.service';
import {Logs} from '../../../models/logs/logs';

@Component({
  selector: 'ngx-prop-trailer-creation',
  templateUrl: './prop-trailer-creation.component.html',
  styleUrls: ['../prop.css'],
})
export class PropTrailerCreationComponent implements OnInit {
  public trailerPropForm: FormGroup;
  @Input() log = new Logs();

  constructor(public fb: FormBuilder,
              public propService: PropertyService) {
    this.trailerPropForm = fb.group({
      suspension: 'ok',
      brakes: 'ok',
      tires_wheels: 'ok',
      hub_seals: 'ok',
      kingpin: 'ok',
      apron: 'ok',
      reflective_tape: 'ok',
      tail_lights: 'ok',
      stop_lights: 'ok',
      turn_signals: 'ok',
      flashers: 'ok',
      strobe_lights: 'ok',
      gap: 'ok',
      landing_gear: 'ok',
      traffic_flags: 'ok',
      oversize_signage: 'ok',
      safety_chains: 'ok',
    });

    this.trailerPropForm.valueChanges.subscribe(property => {
      for (const key in property) {
        if (property.hasOwnProperty(key)) {
          const val = property[key];
          if (val === 'defective') {
            this.propService.addDefectiveKey(key);
          } else if (val === 'na') {
            this.propService.removeDefectiveKey(key);
          } else if (val === 'ok') {
            this.propService.removeDefectiveKey(key);
          }
        }
      }
    });
  }

  ngOnInit() {
    this.propService.setTrailerRadioButtons(this.trailerPropForm);
  }

}
