import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';

@Injectable()
export class PropertyService {
  public defective_properties = [];
  public fixed_properties = [];
  public unit: any;

  public removeDefectiveKey(key) {
    if (this.defective_properties.includes(key) === true) {
      const index = this.defective_properties.indexOf(key, 0);
      if (index > -1) {
        this.defective_properties.splice(index, 1);
      }
    }
    this.addFixedKey(key);
  }

  public addDefectiveKey(key) {
    if (this.defective_properties.includes(key) === false) {
      this.defective_properties.push(key);
      this.removeFixedKey(key);
    }
  }

  public removeFixedKey(key) {
    if (this.fixed_properties.includes(key) === true) {
      const index = this.fixed_properties.indexOf(key, 0);
      if (index > -1) {
        this.fixed_properties.splice(index, 1);
      }
    }
  }

  public addFixedKey(key) {
    if (this.fixed_properties.includes(key) === false) {
      this.fixed_properties.push(key);
    }
  }

  public setTruckRadioButtons(truckPropForm) {
    this.fixed_properties.forEach(
      fixed_key => {
        if (fixed_key !== '') {
          (<FormControl>truckPropForm.controls[fixed_key]).setValue('ok', {onlySelf: true});
        }
      });
    this.defective_properties.forEach(
      key => {
        if (key !== '') {
          if ((<FormControl>truckPropForm.controls[key])) {
            (<FormControl>truckPropForm.controls[key]).setValue('defective', {onlySelf: true});
          }
        }
      });
    this.reset();
  }

  public setTrailerRadioButtons(trailerPropForm) {
    this.fixed_properties.forEach(
      fixed_key => {
        if (fixed_key !== '') {
          (<FormControl>trailerPropForm.controls[fixed_key]).setValue('ok', {onlySelf: true});
        }
      });
    this.defective_properties.forEach(
      key => {
        if (key !== '') {
          if ((<FormControl>trailerPropForm.controls[key])) {
            (<FormControl>trailerPropForm.controls[key]).setValue('defective', {onlySelf: true});
          }
        }
      });
    this.reset();
  }

  public setEquipmentRadioButtons(equipmentPropForm) {
    this.fixed_properties.forEach(
      fixed_key => {
        if (fixed_key !== '') {
          (<FormControl>equipmentPropForm.controls[fixed_key]).setValue('ok', {onlySelf: true});
        }
      });
    this.defective_properties.forEach(
      key => {
        if (key !== '') {
          if ((<FormControl>equipmentPropForm.controls[key])) {
            (<FormControl>equipmentPropForm.controls[key]).setValue('defective', {onlySelf: true});
          }
        }
      });
    this.reset();
  }

  public reset() {
    this.defective_properties = [];
    this.fixed_properties = [];
  }
}
