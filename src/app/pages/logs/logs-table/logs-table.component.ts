import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LogsService} from '../logs.service';
import {LocalDataSource} from 'ng2-smart-table';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'ngx-logs-table',
  templateUrl: './logs-table.component.html',
})

export class LogsTableComponent implements OnInit {
  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      unit: {
        title: 'Unit #:',
        valuePrepareFunction: (unit) => {
          if (unit !== null) {
            return unit.number;
          }
        },
      },
      created_at: {
        title: 'Date',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      job: {
        title: 'Job',
        valuePrepareFunction: (job) => {
          if (job) {
            return job.name;
          }
        },
      },
      deficiency: {title: 'Deficiency'},
    },
  };

  public id: number;
  public logSource: LocalDataSource = new LocalDataSource();

  constructor(private logService: LogsService, private _router: Router, private activatedRoute: ActivatedRoute,
              private datePipe: DatePipe) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.logService.getLogs().subscribe(log_response => this.logSource.load(log_response));
  }

  public onLogClick(event) {
    this._router.navigate(['/pages/logs/logs_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

}
