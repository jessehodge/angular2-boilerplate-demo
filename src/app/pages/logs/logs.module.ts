import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';

import {MessageModule} from '../messages/messages.module';
import {MessageService} from '../messages/messages.service';
import {PropertiesModule} from '../properties/properties.module';
import {UnitService} from '../units/units.service';

import {LogsService} from './logs.service';
import {LogRouting} from './logs.routes';
import {LogsComponent} from './logs.component';
import {LogsCreationComponent} from './logs-creation/logs-creation.component';
import {LogsTableComponent} from './logs-table/logs-table.component';
import {LogsDetailComponent} from './logs-detail/logs-detail.component';
import {ThemeModule} from '../../@theme/theme.module';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LogRouting,
    Ng2SmartTableModule,
    MessageModule,
    PropertiesModule,
    ThemeModule,
    NguiAutoCompleteModule,
  ],
  declarations: [
    LogsComponent,
    LogsCreationComponent,
    LogsTableComponent,
    LogsDetailComponent,
  ],
  providers: [LogsService, MessageService, UnitService, DatePipe],
})
export class LogsModule {
}
