import {Injectable} from '@angular/core';
import {DataService} from '../../services/data-service';

@Injectable()
export class LogsService {

  private _logs_url = this.dataService.base_url + '/logs/';
  private _log_url = this.dataService.base_url + '/log/';
  private _jobs_url = this.dataService.base_url + '/jobs/';

  constructor(private dataService: DataService) {
  }

  // GET
  getJob(id) {
    const url = this._jobs_url + id + '/';
    return this.dataService.get(url);
  }

  getJobs() {
    const url = this._jobs_url;
    return this.dataService.get(url);
  }

  getLog(id) {
    const url = this._log_url + id + '/';
    return this.dataService.get(url);
  }

  getLogs() {
    const url = this._logs_url;
    return this.dataService.get(url);
  }

  // POST
  postLog(log) {
    const url = this._log_url;
    return this.dataService.post(url, log);
  }

  // PUT
  putLog(log) {
    const url = this._logs_url + log.id + '/';
    return this.dataService.patch(url, log);
  }

}
