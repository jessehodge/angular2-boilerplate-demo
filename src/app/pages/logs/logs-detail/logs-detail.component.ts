import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LogsService} from '../logs.service';
import {Logs} from '../../../models/logs/logs';
import {Units} from '../../../models/units/units';
import {WorkOrder} from '../../../models/work-orders/work-order';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-logs-detail',
  templateUrl: './logs-detail.component.html',
})

export class LogsDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public log = new Logs();
  public unit = new Units();
  public work_order = new WorkOrder();
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private logsService: LogsService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.logsService.getLog(this.id).takeUntil(this.ngUnsubscribe).subscribe(res => this.log = res);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
