import {RouterModule, Routes} from '@angular/router';
import {LogsComponent} from './logs.component';
import {LogsTableComponent} from './logs-table/logs-table.component';
import {LogsCreationComponent} from './logs-creation/logs-creation.component';
import {LogsDetailComponent} from './logs-detail/logs-detail.component';

const routes: Routes = [
  {
    path: '',
    component: LogsComponent, children: [
      {path: '', component: LogsTableComponent},
      {path: 'logs_creation', component: LogsCreationComponent},
      {path: 'logs_creation/:id', component: LogsCreationComponent},
      {path: 'logs_detail/:id', component: LogsDetailComponent},
    ],
  }];

export const LogRouting = RouterModule.forChild(routes);
