import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Services
import {ApiService} from '../../../services/api-service';
import {UnitService} from '../../units/units.service';
import {LogsService} from '../logs.service';
import {MessageService} from '../../messages/messages.service';
import {PropertyService} from '../../properties/properties.service';
// Models
import {Properties} from '../../../models/props/properties';
import {Logs} from '../../../models/logs/logs';
import {Units} from '../../../models/units/units';
// Rxjs
import {JobsService} from '../../jobs/jobs.service';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-logs-creation',
  templateUrl: './logs-creation.component.html',
})

export class LogsCreationComponent implements OnInit {
  // Model Objects
  public log = new Logs();
  public prop = new Properties();
  public unit = new Units();
  // Model Arrays
  public jobs = [];
  public operators = [];
  // Public Variables
  public number: string;
  public number_submitted = false;
  public other = false;
  public id: number;

  public user_role = localStorage.getItem('user_role');

  constructor(private logService: LogsService, private apiService: ApiService, private router: Router,
              private messageService: MessageService, private propService: PropertyService, private unitService: UnitService,
              private activatedRoute: ActivatedRoute, private jobService: JobsService,
              private operatorService: UsersService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.number = id;
          this.getUnit(id);
        }
      });
  }

  ngOnInit() {
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
    this.operatorService.getOperators().subscribe(res => this.operators = res.users);
  }

  getUnit(number) {
    this.number_submitted = true;
    this.number = this.unitService.doubleZeroUnitNumber(number);
    this.unitService.getUnitByNumber(this.number).subscribe(res => {
      this.unit = res;
      this.log.unit_id = res.id;
    });
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  public submitLog(log) {
    log.deficient_properties = this.propService.defective_properties.toString();
    if (log.deficient_properties !== '' || this.other === true) {
      log.deficiency = true;
    } else {
      log.deficiency = false;
    }
    log.deficient_properties = this.propService.defective_properties.toString();
    log.job_id = log.job_id.id;
    log.operator_id = log.operator.id;
    this.logService.postLog(log).subscribe(() => this.router.navigate(['/pages/logs/']));
  }

}
