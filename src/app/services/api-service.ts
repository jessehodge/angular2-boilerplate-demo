import {Injectable} from '@angular/core';
import {DataService} from './data-service';

@Injectable()
export class ApiService {
  private _jobs_url = this.data.base_url + '/jobs/';
  private _notifications_url = this.data.base_url + '/notification/';
  private _notification_url = this.data.base_url + '/notifications/confirm/';

  constructor(private data: DataService) {
  }

  getNotifications(id) {
    const url = this._notifications_url + id + '/';
    return this.data.get(url);
  }

  getJobs() {
    const url = this._jobs_url;
    return this.data.get(url);
  }

  updateNotification(notification) {
    const url = this._notification_url + notification.id + '/';
    return this.data.put(url, notification);
  }
}
