import {tokenNotExpired} from 'angular2-jwt';
import {Injectable} from '@angular/core';
import {DataService} from './data-service';
import {Router} from '@angular/router';
import {Http} from '@angular/http';

@Injectable()
export class AuthService {
  private _user_url = this.data.base_url + '/auth/sign_in/';
  private token = localStorage.getItem('token');

  constructor(public data: DataService,
              public router: Router,
              private http: Http) {
  }

  public loggedIn() {
    return tokenNotExpired('token');
  }

  public signIn(user) {
    const url = this._user_url;
    return this.http.post(url, user);
  }

  public signOut() {
    if (this.token) {
      localStorage.clear();
    }
    this.router.navigate(['login']);
  }
}


