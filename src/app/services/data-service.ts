import {Headers, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  public base_url = 'http://192.241.134.129:3000/api/v3';
  json_response = response => <any>(<Response>response).json();
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private authToken: AuthHttp) {
  }

  get(uri) {
    return this.authToken.get(uri, this.headers).map(this.json_response);
  }

  post(uri: string, data?: any) {
    return this.authToken.post(uri, data, this.headers).map(this.json_response);
  }

  patch(uri: string, data?: any) {
    return this.authToken.patch(uri, data, this.headers).map(this.json_response);
  }

  put(uri: string, data?: any) {
    return this.authToken.put(uri, data, this.headers).map(this.json_response);
  }

  delete(uri: string) {
    return this.authToken.delete(uri, this.headers).map(this.json_response);
  }
}
