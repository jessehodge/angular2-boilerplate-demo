export class Notifications {
  public id: number;
  public notify_user_id: any;
  public workorder_id: number;
  public ordered_part_id: number;
  public part_id: any;
  public sender_id: number;
  public escelated_id: number;
  public escelate_at: Date;
  public follow_up_at: Date;
  public confirmed: boolean;
  public confirmed_at: Date;
  public vendor_id: number;
  public call_log_id: number;
  public message: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
