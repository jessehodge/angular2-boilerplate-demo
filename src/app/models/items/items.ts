export class Items {
  id: number;
  number: string;
  description: string;
  inv_value: string;
  qty: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
