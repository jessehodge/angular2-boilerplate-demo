export class CallLogs {
  public id: number;
  public user_id: number;
  public vendor_id: number;
  public message: string;
  public followup_date: Date;
  public workorder_id: number;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
