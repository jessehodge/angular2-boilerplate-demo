import {User} from '../user/user';

export class Messages {
  public date_posted: Date;
  public users: User;
  public message: string;
  public message_type: string;
  public workorder_id: any;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
