import {Locations} from '../locations/locations';

export class Parts {
  id: number;
  item_id: string;
  number: string;
  vendor_id: string;
  unit_cost: string;
  total_cost: string;
  i_location_id: number;
  i_location: Locations;
  unit_id: number;
  qty: string;
  description: string;
  pull_qty: number;
  work_order_id: number;
  requested_by: number;
  location: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
