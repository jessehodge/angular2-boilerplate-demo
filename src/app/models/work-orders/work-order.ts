import {Units} from '../units/units';
import {WorkOrderStatuses} from '../work-order-statuses/work-order-statuses';

export class WorkOrder {
  id: number;
  created_at: string;
  created_by_log: boolean;
  closed_date: string;
  wo_type_id: number;
  wo_type: string;
  unit: Units;
  po_number: number;
  log_number: string;
  workorder_status: WorkOrderStatuses;
  workorder_status_id;
  status: string;
  mechanic_id: number;
  mechanic: string;
  operator_id: number;
  operator: any;
  deficient_properties: string;
  properties_fixed: string;
  vendor_id: number;
  vendor: any;
  meter: number;
  other_description: string;
  unit_exists: boolean;
  phone_number: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
