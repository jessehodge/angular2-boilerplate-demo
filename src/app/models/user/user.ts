import {Jobs} from '../jobs/jobs';

export class User {
  public id: any;
  public email: string;
  public password: string;
  public password_confirmation: string;
  public role_id: string;
  public user_status: string;
  public status: string;
  public user_status_id: number;
  public job_id: string;
  public job: Jobs;
  public role: string;
  public employee_number: string;
  public name: string;
  public roles: any;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
