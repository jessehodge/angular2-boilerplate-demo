export class Prefs {
  public open_workorders: boolean;
  public new_workorders: boolean;
  public old_workorders: boolean;
  public created_units: boolean;
  public requested_parts: boolean;
  public ordered_parts: boolean;
  public received_parts: boolean;
  public id: any;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
