import {Yards} from '../yards/yards';
import {JobStatuses} from './job_status';

export class Jobs {
  public name: string;
  public number: string;
  public admins: string;
  public created_at: Date;
  public updated_at: Date;
  public yard_id: number;
  public unit_count: number;
  public user_count: number;
  public start_date: Date;
  public end_date: Date;
  public foreman: string;
  public contact: string;
  public completion: Date;
  public customers_id: number;
  public job_status: JobStatuses;
  public job_status_id: string;
  public id: number;
  public yard: Yards;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
