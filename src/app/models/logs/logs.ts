import {WorkOrder} from '../work-orders/work-order';

export class Logs {
  id: number;
  created_at: Date;
  created_by: string;
  operator: number;
  posted_by: string;
  unit_id: any;
  date: string;
  meter: string;
  deficiency: boolean;
  deficient_properties: string;
  job_id: any;
  job: any;
  other_description: string;
  unit_exists: boolean;
  unit_number: string;
  workorder: WorkOrder;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
