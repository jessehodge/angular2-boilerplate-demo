export class Properties {
  engine_oil: boolean;
  coolant: boolean;
  transmission: boolean;
  hydraulic_oil: boolean;
  fuel: boolean;
  belts: boolean;
  hood: boolean;
  exhaust: boolean;
  windshield: boolean;
  wiper_washer: boolean;
  mirrors: boolean;
  doors: boolean;
  horn: boolean;
  seat_belts: boolean;
  ac_heat: boolean;
  head_lamps: boolean;
  tail_lights: boolean;
  backup_lights: boolean;
  turn_signals: boolean;
  flashers: boolean;
  stop_lights: boolean;
  clearance_lights: boolean;
  strobe_lights: boolean;
  brakes: boolean;
  drive_line: boolean;
  tires_wheels: boolean;
  rims: boolean;
  hub_seals: boolean;
  lugs: boolean;
  gps_antenna: boolean;
  suspension: boolean;
  extinguisher: boolean;
  triangles: boolean;
  spare_fuses: boolean;
  first_aid: boolean;
  accident_kit: boolean;
  decals: boolean;
  air_filter: boolean;
  fifth_wheel: boolean;
  locking_jaws: boolean;
  release_arm: boolean;
  mounting_bolts: boolean;
  airlines: boolean;
  greased: boolean;
  other: boolean;
  kingpin: boolean;
  reflective_tape: boolean;
  gap: boolean;
  landing_gear: boolean;
  traffic_flags: boolean;
  oversize_signage: boolean;
  safety_chains: boolean;
  undercarriage: boolean;
  backup_alarms: boolean;
  steering: boolean;
  belly_pan: boolean;
  tracks: boolean;
  hydraulic_cylinders: boolean;
  cables: boolean;
  pulleys: boolean;
  apron: boolean;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
