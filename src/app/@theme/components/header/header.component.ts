import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {NbMenuService, NbSidebarService} from '@nebular/theme';
import {ApiService} from '../../../services/api-service';
import {Notifications} from '../../../models/notifications/notifications';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificationsModalComponent} from '../../../pages/modals/notifications/notifications/notifications.modal';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})

export class HeaderComponent implements OnInit {
  @Input() position = 'normal';

  public user: any;
  public logo: any = '';
  public user_id = parseInt(localStorage.getItem('user_id'), 10);

  public notifications = new Array<Notifications>();
  public userMenu = [{title: 'Profile', link: '/pages/profile'}, {title: 'Log out', link: '/pages/login'}];
  public email = localStorage.getItem('uid');

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private router: Router,
              private apiService: ApiService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.apiService.getNotifications(this.user_id)
      .subscribe(res => {
        this.notifications = res;
      });
    // this.logo = 'assets/images/logo.jpg';
    // if (this.authToken.userSignedIn()) {
    //   this.user = this.authToken.currentUserData;
    // } else {
    //   this.logOut();
    // }
  }

  logOut() {
    localStorage.clear();
    // this.authToken.signOut();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  // goToMail() {this.router.navigate(['/pages/mail']); }

  openNotificationModal() {
    this.modalService.open(NotificationsModalComponent);
  }
}
